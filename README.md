# To-do List Web App
An back-end server for simple task management.

## Project setup
This is the only the back-end setup.
Once the server is up and running, it will listen on http://localhost:3000.

```
yarn install
```

### Starts server
```
yarn run serve
```

## Notes
As expected the application is very basic. Reading/writing operations are performed on JSON files as per requirements.
While this is a convenient solution for a quick implementation, it's not recommended due to security, performance, concurrency, conflict (and other) reasons.

In particular:

- CRUD operations are implemented.
- Authentication is token-based and the client-side token is saved in LocalStorage.
- Authentication expires in 30 minutes.
- Password encryption is implemented to show that there is a need for securing sensible data, with no ambition to claim that the solution here in place is enough to guarantee security.

## Implementtion Notes
- User passwords get encrypted (with bcrypt - secrete key stored in ./.env file) and stored into user json data.
- Entry ids (both for users and tasks) get generated progressively keeping track of the last used id through a dedicated property (lastID) in the json file (similarly to auto-increment fields in databases).
- Architecture is RESTful.

## Techology stack

- Node.js
- Express.js
