const Tasks = require('../Model/Tasks');

const TaskController = {
  create: (request, response) => {
    const { id: userId } = request.user;
    const { title, note, dueDate, priority, isComplete } = request.body;

    if (!userId) {
      response.sendStatus(401);
    }

    const tasks = new Tasks();
    const task = tasks.save({
      dueDate,
      isComplete,
      note,
      priority,
      title,
      userId,
    });
    response.send(task);
  },
  delete: (request, response) => {
    const { id: userId } = request.user;
    const id = +request.params.id;

    if (!userId) {
      response.sendStatus(401);
    }

    if (!id) {
      response.sendStatus(400);
    }

    const tasks = new Tasks();
    const task = tasks.find({ id, userId });
    tasks.delete(task);
    response.send(task);
  },
  index: (request, response) => {
    const { id: userId } = request.user;

    if (!userId) {
      response.sendStatus(401);
    }

    const tasks = new Tasks();
    response.send(tasks.findAll({ userId }));
  },
  update: (request, response) => {
    const { id: userId } = request.user;
    const data = request.body;

    if (!userId) {
      response.sendStatus(401);
    }

    const tasks = new Tasks();
    const task = tasks.save(data);
    response.send(task);
  },
};

module.exports = TaskController;
