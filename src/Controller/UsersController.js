const bcrypt = require('bcrypt');

const { generateToken } = require('../auth');
const Users = require('../Model/Users');

const UserController = {
  signin: (request, response) => {
    const { email, username, password } = request.body;
    const isUserDefined = !!(username || email);

    if (!isUserDefined || !password) {
      return response.sendStatus(400);
    }

    const users = new Users();
    let condition = { email, username };
    condition = Object.keys(condition).reduce((acc, key) => {
      if (condition[key]) {
        acc[key] = condition[key];
      }

      return acc;
    }, {});

    const user = users.find(condition);

    if (!user || !bcrypt.compareSync(password, user.password)) {
      return response.sendStatus(401);
    }

    return response.send({ token: generateToken(user) });
  },
  signup: (request, response) => {
    const { username, password } = request.body;

    if (!username || !password) {
      return response.sendStatus(400);
    }

    const users = new Users();
    let user = users.find({ username });

    if (user) {
      return response.sendStatus(422);
    }

    user = users.save({ ...request.body, password: bcrypt.hashSync(password, 10) });
    return response.send({ token: generateToken(user) });
  },
};

module.exports = UserController;
