const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const TMP = path.resolve(__dirname, '../../tmp');

class JsonModel {
  $data = [];
  $fileStorage = null;
  $lastID = 0;

  columns = ['id', 'name'];

  constructor() {
    this.$fileStorage = path.resolve(TMP, `${this.constructor.name}.json`);
    this.loadFromFS();
  }

  delete(row) {
    const index = this.$data.findIndex(dataRow => Object.keys(row).every(key => dataRow[key] == row[key]));
    if (index >= 0) {
      this.$data.splice(index, 1);
      this.writeToFS();
    }
    return false;
  }

  get(id) {
    return this.find({ id });
  }

  find(condition) {
    if (!this.$data || !this.$data.length) return null;

    return this.normalizeRow(this.$data.find((dataRow) => {
      return Object.keys(condition).every(key => dataRow[key] === condition[key]);
    }));
  }

  findAll(condition) {
    if (!this.$data || !this.$data.length) return [];

    return this.$data.filter((dataRow) => {
      return Object.keys(condition).every(key => dataRow[key] === condition[key]);
    }).map(row => this.normalizeRow(row));
  }

  save(data) {
    const index = this.$data.findIndex(row => row.id == data.id);
    if (index === -1) {
      data.id = +this.$lastID + 1;
      this.$lastID = data.id;
      this.$data.push(this.normalizeRow(data));
    } else {
      this.$data.splice(index, 1, this.normalizeRow(data));
    }

    // Not efficient to write on any change, but fine for this couple of entries
    this.writeToFS();
    return data;
  }

  // Helpers
  normalizeRow(row) {
    if (!row) return row;

    return Object.keys(row).reduce((acc, key) => {
      if (this.columns.includes(key)) {
        acc[key] = row[key];
      }

      return acc;
    }, {});
  }

  // FS operations
  loadFromFS() {
    if (!fs.existsSync(this.$fileStorage)) {
      console.log(chalk.yellowBright('Creating file storage for model: %s'), chalk.cyanBright(this.constructor.name));
      this.writeToFS();
    }

    const { data, lastID } = JSON.parse(fs.readFileSync(this.$fileStorage, 'utf-8'));

    this.$data = data;
    this.$lastID = lastID;
  }

  writeToFS() {
    const data = {
      data: this.$data,
      lastID: this.$lastID,
      modified: +Date.now(),
    };

    if (!fs.existsSync(TMP)) fs.mkdirSync(TMP);

    fs.writeFileSync(this.$fileStorage, JSON.stringify(data), 'utf8');
  }
}

module.exports = JsonModel;
