const JsonModel = require("./JsonModel");

class Tasks extends JsonModel {
  columns = [
    'dueDate',
    'id',
    'isComplete',
    'note',
    'priority',
    'title',
    'userId',
  ];
}

module.exports = Tasks;
