const bcrypt = require('bcrypt');
const JsonModel = require("./JsonModel");

class Users extends JsonModel {
  columns = [
    'email',
    'id',
    'password',
    'username',
  ];
}

module.exports = Users;
