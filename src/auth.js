const chalk = require('chalk');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

dotenv.config();

const SECRET_KEY = process.env.SECRET_KEY;
const TOKEN_EXPIRES_IN = '1800s';

function authenticateToken(request, response, next) {
  const authHeader = request.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  
  if (token == null) {
    return response.sendStatus(401);
  }

  jwt.verify(token, SECRET_KEY, (err, user) => {
    if (err) {
      return response.sendStatus(401);
    }

    request.user = user;
    next();
  })
}

function generateToken({ id, username }) {
  console.log(chalk.yellowBright('Generating token for user:'), chalk.cyanBright(username));

  return jwt.sign({ id, username }, SECRET_KEY, { expiresIn: TOKEN_EXPIRES_IN });
}

module.exports = {
  authenticateToken,
  generateToken,
};
