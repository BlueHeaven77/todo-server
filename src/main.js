const chalk = require('chalk');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const router = require('./router');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api', router);

console.log(chalk.bgCyanBright.black(' LISTENING '), chalk.cyanBright('http://localhost:3000'));
app.listen(3000);
