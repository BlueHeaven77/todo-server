const express = require('express');
const { authenticateToken } = require('./auth');
const Task = require('./Controller/TaskController');
const User = require('./Controller/UsersController');

const router = express.Router();

router.post('/login', User.signin);
router.post('/signin', User.signin);
router.post('/signup', User.signup);

router.use('/tasks/', authenticateToken);
router.delete('/tasks/:id', Task.delete);
router.get('/tasks/', Task.index);
router.post('/tasks/', Task.create);
router.put('/tasks/', Task.update);

module.exports = router;
